pragma solidity ^0.4.19;

// mettre la déclaration d'importation ici
import "./zombiefactory.sol";

// Interface declaration
contract KittyInterface {
  function getKitty(uint256 _id) external view returns (
    bool isGestating,
    bool isReady,
    uint256 cooldownIndex,
    uint256 nextActionAt,
    uint256 siringWithId,
    uint256 birthTime,
    uint256 matronId,
    uint256 sireId,
    uint256 generation,
    uint256 genes
  );
}

contract ZombieFeeding is ZombieFactory {

	// Interface declaration & attribute a value
	address ckAddress = 0x06012c8cf97BEaD5deAe237070F9587f8E7A266d;
	KittyInterface kittyContract = KittyInterface(ckAddress);

	// require usage to check if the consumer is the zombie owner
	// storage keyword usage to store the current definitely zombie into blockchain
	function feedAndMultiply(uint _zombieId, uint _targetDna) public {
		require (msg.sender == zombieToOwner[_zombieId]);
		Zombie storage myZombie = zombies[_zombieId];

		// here we will calculate dna of the new created zombie
		// the new dna will be aqual to the average 
		_targetDna = _targetDna % dnaModulus;
		uint newDna = (myZombie.dna + _targetDna) / 2;
		_createZombie("NoName", newDna);
	}

}
